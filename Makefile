CC=gcc
CFLAGS=-O3 -std=c99 -D_DEFAULT_SOURCE
PYTHON_INC=/usr/include/python3.8

PY_BIN_FILES:=$(patsubst src/%.py,bin/%.py,$(wildcard src/*.py))
PY_DEV_FILES:=$(patsubst src/%.py,dev/%.py,$(wildcard src/*.py))

VALGRIND_SUPPRESSION_BIN_FILES:=$(patsubst src/%.supp,bin/%.supp,$(wildcard src/*.supp))
VALGRIND_SUPPRESSION_DEV_FILES:=$(patsubst src/%.supp,dev/%.supp,$(wildcard src/*.supp))

all: bin_dir $(PY_BIN_FILES) bin/accelerated_functions.so $(VALGRIND_SUPPRESSION_BIN_FILES)
dev: dev_dir $(PY_DEV_FILES) dev/accelerated_functions.so $(VALGRIND_SUPPRESSION_DEV_FILES)

clean:
	@if [ -d bin ]; then rm -r bin; fi
	@if [ -d dev ]; then rm -r dev; fi

bin/accelerated_functions.so: src/accelerated_functions.c
dev/accelerated_functions.so: src/accelerated_functions.c

#######

bin/%.so: src/%.c
	$(CC) $(CFLAGS) -I $(PYTHON_INC) -shared -fPIC $^ -o $@

bin/%.py: src/%.py
	@cp $< $@

bin/%.supp: src/%.supp
	@cp $< $@

bin_dir:
	@mkdir -p bin

#######

dev/%.so: src/%.c
	$(CC) $(CFLAGS) -I $(PYTHON_INC) -shared -fPIC $^ -o $@

dev/%.py: src/%.py
	rm -f $@
	ln $< $@

dev/%.supp: src/%.supp
	rm -f $@
	ln $< $@

dev_dir:
	@mkdir -p dev
