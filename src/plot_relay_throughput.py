import matplotlib.pylab as plt
import numpy as np
import math
import sys
#
import data_helpers
#
#filenames = ['relay_throughput.log', 'multithreaded-throughput-rpi.log']
#filenames = ['cluck2_working_0_0.log', 'original-throughput.log']
filenames = sys.argv[1:]
#
selected_data = {}
for name in filenames:
	(_, data) = data_helpers.read_relay_throughput(name)
	#
	totals = np.sum(data['threads'], axis=1)
	start_time = np.where(totals/2**20 > 5)[0][0]-10
	end_time = np.where(totals/2**20 > 5)[0][-1]+10
	#
	selected_data[name] = {}
	selected_data[name]['timestamps'] = data['timestamps'][start_time:end_time]
	selected_data[name]['timestamps'] -= selected_data[name]['timestamps'][0]
	selected_data[name]['threads'] = data['threads'][start_time:end_time,:]
	#
	normalized_data = data_helpers.normalize_relay_throughput(data)
	selected_data[name]['timestamps_normalized'] = normalized_data['timestamps'][start_time:end_time]
	selected_data[name]['threads_normalized'] = normalized_data['threads'][start_time:end_time,:]
	#
	time_deltas = selected_data[name]['timestamps'][1:]-selected_data[name]['timestamps'][:-1]
	num_timesteps = int(round(30/np.mean(time_deltas)))
	max_throughput = np.max(np.convolve(0.5*np.sum(selected_data[name]['threads_normalized'], axis=1)/2**20, np.ones(num_timesteps)/num_timesteps, 'valid'))
	# we divide by 2 since we the throughput is the average of the sent and received bytes
	print('{}: {:.2f} MiB/s'.format(name, max_throughput))
#
for name in selected_data:
	plt.step(selected_data[name]['timestamps_normalized'], 0.5*np.sum(selected_data[name]['threads_normalized'], axis=1)/2**20, label=name, where='post')
	for thread in range(int(selected_data[name]['threads'].shape[1]/2)):
		for (ind, direction) in zip(range(2), ['sent', 'recv']):
			label = '{} - {} - {}'.format(name, thread, direction)
			#plt.plot(selected_data[name]['timestamps_normalized'], selected_data[name]['threads_normalized'][:,2*thread+ind]/2**20, label=label)
			#plt.plot(selected_data[name]['timestamps_normalized'], np.cumsum(selected_data[name]['threads_normalized'][:,2*thread+ind])/2**20, label=label)
		#
	#
	for (ind, direction) in zip(range(2), ['sent', 'recv']):
		label = '{} - {}'.format(name, direction)
		#plt.plot(selected_data[name]['timestamps_normalized'], np.sum(selected_data[name]['threads_normalized'][:,ind::2], axis=1)/2**20, label=label)
		#plt.plot(selected_data[name]['timestamps_normalized'], np.cumsum(np.sum(selected_data[name]['threads_normalized'][:,ind::2], axis=1))/2**20, label=label)
	#
#
plt.xlabel('Time (s)')
plt.ylabel('Throughput (MiB/s)')
plt.legend()
plt.show()
