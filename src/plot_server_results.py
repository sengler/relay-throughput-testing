import sys
import numpy as np
import matplotlib.pylab as plt
import pdb
#
import data_helpers
#
if __name__ == '__main__':
	transfers = data_helpers.read_server_results(sys.argv[1])
	clients = data_helpers.read_client_info(sys.argv[2])
	#
	#approx_start_time = np.min([x['time_of_first_byte'] for x in transfers])
	approx_start_time = clients['start_time']
	#
	colormap = plt.get_cmap('tab20').colors #'tab10'
	assigned_colors = []
	#
	for transfer in transfers:
		if transfer['custom_data']['circuit'][1][-1] in assigned_colors:
			color_index = assigned_colors.index(transfer['custom_data']['circuit'][1][-1])
		else:
			color_index = len(assigned_colors)
			assigned_colors.append(transfer['custom_data']['circuit'][1][-1])
		#
		plt.step(transfer['deltas']['timestamps']-approx_start_time, np.cumsum(transfer['deltas']['bytes']),
		         color=colormap[color_index%len(colormap)], where='post')
	#
	ax1 = plt.gca()
	ax1.set_ylabel('Bytes')
	ax2 = ax1.twinx()
	ax2.set_ylim([x/(2**20) for x in ax1.get_ylim()])
	ax2.set_ylabel('MiB')
	plt.show()
	#
	last_byte_times = np.array([x['time_of_last_byte'] for x in transfers])
	plt.plot(np.sort(last_byte_times)-approx_start_time, np.arange(len(last_byte_times)))
	plt.xlim([0, None])
	plt.ylim([0, None])
	plt.show()
#
