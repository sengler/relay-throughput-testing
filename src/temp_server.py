#!/usr/bin/env python3
#
import logging
import argparse
import multiprocessing
#
import throughput_server
#
if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG)
	#
	parser = argparse.ArgumentParser(description='Test the network throughput (optionally through a proxy).')
	parser.add_argument('port', type=int, help='listen on port')
	parser.add_argument('--localhost', action='store_true', help='bind to 127.0.0.1 instead of 0.0.0.0')
	args = parser.parse_args()
	#
	if args.localhost:
		bind_to = ('127.0.0.1', args.port)
	else:
		bind_to = ('0.0.0.0', args.port)
	#
	stop_event = multiprocessing.Event()
	server = throughput_server.ThroughputServer(bind_to, None)
	try:
		server.run()
	except KeyboardInterrupt:
		print('')
		logging.debug('Server stopped (KeyboardInterrupt).')
	#
	results = server.results
	#
	for x in results:
		logging.info('{}'.format(x['results']['custom_data']))
	#
#
