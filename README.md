# Relay Throughput Testing

This repository contains scripts to test the throughput of a Tor relay. It requires a [patched version of Chutney](https://git-crysp.uwaterloo.ca/sengler/chutney-for-relay-testing) and [Stem](https://gitweb.torproject.org/stem.git).

Example:

```bash
git clone https://git-crysp.uwaterloo.ca/sengler/chutney-for-relay-testing.git
git checkout paper
mv chutney-for-relay-testing chutney

git clone https://git-crysp.uwaterloo.ca/sengler/tor-parallel-relay-conn.git
git checkout paper
mv tor-parallel-relay-conn tor

ssh-keygen -b 2048 -t rsa -f sshkey -q -N ""

sudo DOCKER_BUILDKIT=1 docker build --tag experiment-benchmarker --target benchmarker .
sudo DOCKER_BUILDKIT=1 docker build --tag experiment-controller --target controller .

sudo docker run --init --name benchmarker -dit --hostname benchmarker --network host experiment-benchmarker /bin/sh -c "service ssh start && exec /bin/sh"
sudo docker run --init --name controller -dit --hostname controller --network host --volume /tmp/results:/results experiment-controller /bin/sh

sudo docker exec -it controller /bin/bash -c 'cd "$HOME" && exec /bin/bash'
  cd ~/code/working/tor-benchmarking/bin
  touch /tmp/nothing
  ssh 127.0.0.1 /bin/true # accept key fingerprint
  ssh localhost /bin/true # accept key fingerprint
  # modify relay_working_experiment.py as needed
  python3 -u relay_working_experiment.py 1B --target-tor /tmp/nothing > /tmp/out.log 2>&1
  less +F /tmp/out.log

sudo docker rm -f controller
sudo docker rm -f benchmarker
```
