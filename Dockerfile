FROM ubuntu:20.04 AS base

RUN apt-get update \
 && apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        wget \
        build-essential \
        automake \
        autoconf \
        libtool \
        python3 \
        python3-numpy \
        zlib1g \
        zlib1g-dev \
        tmux \
        vim \
        dstat \
        htop

RUN mkdir ~/code \
          ~/code/releases \
          ~/code/dev \
          ~/code/working \
          ~/build

RUN cd /tmp \
 && wget --no-verbose https://github.com/openssl/openssl/archive/OpenSSL_1_1_1h.tar.gz \
 && echo "d1f723c1f6b6d1eaf26655caa50d2f60d4d33f4b04977b1da63def878f386fcc  OpenSSL_1_1_1h.tar.gz" | sha256sum -c - \
 && tar -xzf OpenSSL_*.tar.gz \
 && rm OpenSSL_*.tar.gz \
 && mv openssl-OpenSSL_* ~/code/releases

RUN cd ~/code/releases/openssl-OpenSSL_* \
 && ./config --shared --prefix="$HOME/build" \
 && make \
 && make install \
 && [ -f "$HOME/build/lib/libssl.so" ] && echo "OK" || echo "Not installed properly"

RUN cd /tmp \
 && wget --no-verbose https://github.com/libevent/libevent/releases/download/release-2.1.12-stable/libevent-2.1.12-stable.tar.gz \
 && echo "92e6de1be9ec176428fd2367677e61ceffc2ee1cb119035037a27d346b0403bb  libevent-2.1.12-stable.tar.gz" | sha256sum -c - \
 && tar -xzf libevent-2.1.12-stable.tar.gz \
 && rm libevent-2.1.12-stable.tar.gz \
 && mv libevent-*-stable ~/code/releases

RUN cd ~/code/releases/libevent-*-stable \
 && ./configure --prefix="$HOME/build" --disable-openssl \
 && make "-j$(nproc)" \
 && make install

RUN cd /tmp \
 && wget --no-verbose https://github.com/gperftools/gperftools/archive/gperftools-2.7.tar.gz \
 && echo "3a88b4544315d550c87db5c96775496243fb91aa2cea88d2b845f65823f3d38a  gperftools-2.7.tar.gz" | sha256sum -c - \
 && tar -xzf gperftools-*.tar.gz \
 && rm gperftools-*.tar.gz \
 && mv gperftools-gperftools-* ~/code/releases

RUN cd ~/code/releases/gperftools-gperftools-* \
 && ./autogen.sh \
 && ./configure --prefix="$HOME/build" \
 && make "-j$(nproc)" \
 && make install

RUN cd /tmp \
 && wget --no-verbose https://github.com/jemalloc/jemalloc/archive/5.2.1.tar.gz \
 && mv 5.2.1.tar.gz jemalloc-5.2.1.tar.gz \
 && echo "ed51b0b37098af4ca6ed31c22324635263f8ad6471889e0592a9c0dba9136aea  jemalloc-5.2.1.tar.gz" | sha256sum -c - \
 && tar -xzf jemalloc-*.tar.gz \
 && rm jemalloc-*.tar.gz \
 && mv jemalloc-5.2.1 ~/code/releases

RUN cd ~/code/releases/jemalloc-* \
 && ./autogen.sh \
 && ./configure --prefix="$HOME/build" \
 && make "-j$(nproc)" \
 && make install

RUN cd /tmp \
 && wget --no-verbose https://archive.torproject.org/tor-package-archive/tor-0.4.2.6.tar.gz \
 && echo "0500102433849bbe3231c590973d126c2d2d6b3943b4b9f9962bdb108436e6c4  tor-0.4.2.6.tar.gz" | sha256sum -c - \
 && tar -xzf tor-0.4.2.6.tar.gz \
 && rm tor-0.4.2.6.tar.gz \
 && mv tor-0.4.2.6 ~/code/releases/tor-0.4.2.6

RUN cd ~/code/releases/tor-0.4.2.6 \
 && ./configure --disable-asciidoc --with-libevent-dir="$HOME/build" --with-openssl-dir="$HOME/build" \
 && LD_RUN_PATH="$HOME/build/lib" make "-j$(nproc)" \
 && ldd src/app/tor

COPY tor /root/code/working/tor

RUN cd ~/code/working/tor \
 && ./autogen.sh \
 && ./configure --disable-asciidoc --disable-unittests --with-libevent-dir="$HOME/build" --with-openssl-dir="$HOME/build" \
 && LD_RUN_PATH="$HOME/build/lib" make "-j$(nproc)" \
 && ldd src/app/tor

########################

FROM base AS benchmarker

RUN apt-get update \
 && apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        openssh-server

COPY sshkey.pub /tmp/sshkey.pub

RUN sed -i 's/#\?Port 22/Port 2222/g' /etc/ssh/sshd_config \
 && mkdir ~/.ssh \
 && chmod 700 ~/.ssh \
 && cat /tmp/sshkey.pub >> ~/.ssh/authorized_keys \
 && chmod 600 ~/.ssh/authorized_keys \
 && rm /tmp/sshkey.pub

RUN cd /tmp \
 && wget --no-verbose https://archive.torproject.org/tor-package-archive/tor-0.4.2.6.tar.gz \
 && echo "0500102433849bbe3231c590973d126c2d2d6b3943b4b9f9962bdb108436e6c4  tor-0.4.2.6.tar.gz" | sha256sum -c - \
 && tar -xzf tor-0.4.2.6.tar.gz \
 && rm tor-0.4.2.6.tar.gz \
 && mv tor-0.4.2.6 ~/code/dev/tor-0.4.2.6-throughput-log

COPY src/throughput-logging.patch /tmp

RUN cd ~/code/dev/tor-0.4.2.6-throughput-log \
 && patch -p1 < /tmp/throughput-logging.patch \
 && rm /tmp/throughput-logging.patch \
 && ./configure --disable-asciidoc --with-libevent-dir="$HOME/build" --with-openssl-dir="$HOME/build" \
 && LD_RUN_PATH="$HOME/build/lib" make "-j$(nproc)" \
 && ldd src/app/tor

########################

FROM base as controller

RUN apt-get update \
 && apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        git \
        python3-dev \
        python3-pip \
        openssh-client

COPY sshkey /root/.ssh/id_rsa

RUN printf "\nHost *\n    Port 2222\n    StrictHostKeyChecking no\n" >> ~/.ssh/config

RUN cd ~/code/dev \
 && git clone https://git.torproject.org/stem.git \
 && cd stem \
 && git checkout '1.8.0' \
 && pip3 install --user .

COPY chutney /root/code/working/chutney

RUN printf 'export CHUTNEY_DATA_DIR="/tmp/chutney-net"\n' >> ~/.bashrc

RUN cd /tmp \
 && wget --no-verbose https://archive.torproject.org/tor-package-archive/tor-0.4.2.6.tar.gz \
 && echo "0500102433849bbe3231c590973d126c2d2d6b3943b4b9f9962bdb108436e6c4  tor-0.4.2.6.tar.gz" | sha256sum -c - \
 && tar -xzf tor-0.4.2.6.tar.gz \
 && rm tor-0.4.2.6.tar.gz \
 && mv tor-0.4.2.6 ~/code/dev/tor-0.4.2.6-fixed-controller

COPY src/fixed-controller.patch /tmp

RUN cd ~/code/dev/tor-0.4.2.6-fixed-controller \
 && patch -p1 < /tmp/fixed-controller.patch \
 && rm /tmp/fixed-controller.patch \
 && ./configure --disable-asciidoc --with-libevent-dir="$HOME/build" --with-openssl-dir="$HOME/build" \
 && LD_RUN_PATH="$HOME/build/lib" make "-j$(nproc)" \
 && ldd src/app/tor

COPY . /root/code/working/tor-benchmarking

RUN cd ~/code/working/tor-benchmarking \
 && make
